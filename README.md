## Tutorial #01: Getting Started with MuBu

Please look at the patches inside - [Ircam_Forum-MuBu-Tutorial-#01](https://git.forum.ircam.fr/beller/mubu-tutorials-material/-/tree/master/Ircam_Forum-MuBu-Tutorial-%2301)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/YvcotAZAXfQ/0.jpg)](https://www.youtube.com/watch?v=YvcotAZAXfQ)

## Tutorial #02: Getting Data into a MuBu

Please look at the patches inside - [Ircam_Forum-MuBu-Tutorial-#02](https://git.forum.ircam.fr/beller/mubu-tutorials-material/-/tree/master/Ircam_Forum-MuBu-Tutorial-%2302)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/M1aWlSTWgNk/0.jpg)](https://www.youtube.com/watch?v=M1aWlSTWgNk)

## Tutorial #03: Granular Synthesis using MuBu

Please look at the patches inside - [Ircam_Forum-MuBu-Tutorial-#03](https://git.forum.ircam.fr/beller/mubu-tutorials-material/-/tree/master/Ircam_Forum-MuBu-Tutorial-%2303)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/FP24iQNAzmU/0.jpg)](https://www.youtube.com/watch?v=FP24iQNAzmU)

## Tutorial #04: Audio Analysis in MuBu

Please look at the patches inside - [Ircam_Forum-MuBu-Tutorial-#04](https://git.forum.ircam.fr/beller/mubu-tutorials-material/-/tree/master/Ircam_Forum-MuBu-Tutorial-%2304)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/y-LehPo1f4o/0.jpg)](https://www.youtube.com/watch?v=y-LehPo1f4o)

Please [download](https://git.forum.ircam.fr/beller/mubu-tutorials-material/) the project to get the patches.

MuBu -- A toolbox for multimodal analysis of sound and motion, interactive sound synthesis and machine learning
https://forum.ircam.fr/projects/detail/mubu/

Developed at IRCAM by the Sound Music Movement Interaction Team
https://ismm.ircam.fr/ https://ircam-ismm.github.io/

The [ISMM Team](https://www.stms-lab.fr/team/interaction-son-musique-mouvement) is part of the STMS Laboratory (Sciences et Technologies de la Musique et du Son) Ircam–CNRS UMR 9912—Sorbonne Université http://stms-lab.fr/

Design and presentation by Matthew Ostrowski
http://ostrowski.info
