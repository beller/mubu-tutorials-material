{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 3,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"openrect" : [ 34.0, 87.0, 1724.0, 883.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 14.0,
		"default_fontface" : 0,
		"default_fontname" : "Monaco",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 1724.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "IRCAM",
		"subpatcher_template" : "monaco_template",
		"showontab" : 1,
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 171.040169133192393, 339.0, 244.0, 26.0 ],
					"text" : "07_frequency_matching.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 151.547568710359428, 305.0, 269.0, 26.0 ],
					"text" : "06_segmenting_with_pitch.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 133.522198731501078, 272.0, 294.0, 26.0 ],
					"text" : "05_segmenting_with_loudness.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 121.689217758985194, 239.0, 193.0, 26.0 ],
					"text" : "04_yin_analysis.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 107.771670190274847, 207.0, 227.0, 26.0 ],
					"text" : "03_chaining_modules.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 94.344608879492597, 175.0, 219.0, 26.0 ],
					"text" : "02_data_filters.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 83.492600422832979, 144.0, 177.0, 26.0 ],
					"text" : "01_pipo_intro.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 67.0, 111.0, 269.0, 26.0 ],
					"text" : "00_mubu-analysis-credits.maxpat"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 9.0,
					"hidden" : 1,
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1498.0, 749.0, 69.0, 20.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 9.0,
					"hidden" : 1,
					"id" : "obj-16",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1498.0, 774.666666666666742, 162.0, 31.0 ],
					"text" : "window size 34 87 1758 970, window exec"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 9.0,
					"hidden" : 1,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1498.0, 809.333333333333371, 94.0, 20.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"hidden" : 1,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"hidden" : 1,
					"source" : [ "obj-17", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-5::obj-14" : [ "live.gain~", "live.gain~", 0 ],
			"obj-5::obj-25" : [ "slider", "slider", 0 ],
			"obj-5::obj-65" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-6::obj-22" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-7::obj-22" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-7::obj-4::obj-22" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-8::obj-14" : [ "live.gain~[6]", "live.gain~", 0 ],
			"obj-8::obj-25" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-8::obj-50" : [ "slider[1]", "balance", 0 ],
			"obj-8::obj-59" : [ "umenu", "umenu", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-6::obj-22" : 				{
					"parameter_longname" : "live.gain~[2]"
				}
,
				"obj-7::obj-22" : 				{
					"parameter_longname" : "live.gain~[4]"
				}
,
				"obj-7::obj-4::obj-22" : 				{
					"parameter_longname" : "live.gain~[3]"
				}
,
				"obj-8::obj-14" : 				{
					"parameter_longname" : "live.gain~[6]"
				}
,
				"obj-8::obj-25" : 				{
					"parameter_initial" : 0,
					"parameter_longname" : "live.gain~[5]"
				}
,
				"obj-8::obj-50" : 				{
					"parameter_longname" : "slider[1]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "00_mubu-analysis-credits.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "01_pipo_intro.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "02_data_filters.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "03_chaining_modules.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "04_yin_analysis.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "05_segmenting_with_loudness.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "06_segmenting_with_pitch.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "07_frequency_matching.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "20170816 - Bloc-marque Culture couleur.tiff",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "IRCAM.CP.tiff",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO CNRS 2019_CMJN.tiff",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO_SU_HORIZ_SEUL_CMJN color.tiff",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "M4L.bal2~.maxpat",
				"bootpath" : "C74:/patchers/m4l/Tools resources",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "duduk.aif",
				"bootpath" : "C74:/media/msp",
				"type" : "AIFF",
				"implicit" : 1
			}
, 			{
				"name" : "girl_singing.wav",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals/audio",
				"patcherrelativepath" : "./audio",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "iMubu-large.png",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_02/old lesson 2/media",
				"patcherrelativepath" : "../old lesson 2/media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "imubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "logo-ircamforum.png",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_02/old lesson 2/media",
				"patcherrelativepath" : "../old lesson 2/media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "mubu logo.tiff",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "mubu.concat~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.granular~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.knn.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.play~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.process.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.record.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.record~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.track.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "pipo.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "pipo~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "the_flea.wav",
				"bootpath" : "~/Documents/Max 8/clients/IRCAM documentation/MuBu_lesson_patches/lesson_04/MuBu Lesson 4 finals/audio",
				"patcherrelativepath" : "./audio",
				"type" : "WAVE",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "IRCAM",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "monacy",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 9.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
