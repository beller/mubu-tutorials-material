{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 2,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"openrect" : [ 34.0, 87.0, 1724.0, 883.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 14.0,
		"default_fontface" : 0,
		"default_fontname" : "Monaco",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 1724.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "IRCAM",
		"subpatcher_template" : "monaco_template",
		"showrootpatcherontab" : 0,
		"showontab" : 0,
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 30.0, 66.0, 202.0, 26.0 ],
					"text" : "mubu-rec-credits.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 193.0, 126.0, 151.0, 26.0 ],
					"text" : "mubu-refer.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 219.0, 157.0, 210.0, 26.0 ],
					"text" : "mubu-record-audio.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 270.0, 190.0, 250.0, 26.0 ],
					"text" : "mubu-rec-audio-data.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 106.0, 98.0, 185.0, 26.0 ],
					"text" : "mubu-readaudio.maxpat"
				}

			}
 ],
		"lines" : [  ],
		"dependency_cache" : [ 			{
				"name" : "20170816 - Bloc-marque Culture couleur.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "IRCAM.CP.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO CNRS 2019_CMJN.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO_SU_HORIZ_SEUL_CMJN color.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "iMubu-large.png",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/z-logos",
				"patcherrelativepath" : "../z-logos",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "logo-ircamforum.png",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/z-logos",
				"patcherrelativepath" : "../z-logos",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "mubu logo.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-readaudio.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson_02",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-rec-audio-data.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson_02",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-rec-credits.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson_02",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-record-audio.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson_02",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-refer.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson_02",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "pipo.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "IRCAM",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "monacy",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 9.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
