{
	"name" : "mubu_granular_patches",
	"version" : 1,
	"creationdate" : 3732882079,
	"modificationdate" : 3733160247,
	"viewrect" : [ 25.0, 105.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"grain-visualization.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"bonus recorder.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Mubu_granular_patches.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"media" : 		{

		}
,
		"externals" : 		{

		}
,
		"other" : 		{
			"grantester.mubu" : 			{
				"kind" : "file",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
