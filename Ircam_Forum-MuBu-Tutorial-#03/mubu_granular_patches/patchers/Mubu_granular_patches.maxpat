{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 3,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 37.0, 89.0, 1724.0, 883.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 14.0,
		"default_fontface" : 0,
		"default_fontname" : "Monaco",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 1724.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "IRCAM",
		"subpatcher_template" : "monaco_template",
		"showrootpatcherontab" : 0,
		"showontab" : 0,
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 450.0, 267.0, 202.0, 26.0 ],
					"text" : "\"bonus recorder.maxpat\"",
					"varname" : "bonus recorder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 331.0, 226.0, 230.0, 26.0 ],
					"text" : "grain-visualization.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 51.0, 88.0, 210.0, 26.0 ],
					"text" : "mubu-play-credits.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 270.0, 192.0, 185.0, 26.0 ],
					"text" : "mubu-concat.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 151.0, 156.0, 254.0, 26.0 ],
					"text" : "mubu-live-granular.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 106.0, 126.0, 202.0, 26.0 ],
					"text" : "mubu-granular.maxpat"
				}

			}
 ],
		"lines" : [  ],
		"parameters" : 		{
			"obj-1::obj-15" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-1::obj-27" : [ "number[1]", "number[1]", 0 ],
			"obj-1::obj-53" : [ "number", "number", 0 ],
			"obj-4::obj-19::obj-39" : [ "Loop", "Loop", 0 ],
			"obj-4::obj-19::obj-50" : [ "Play", "Play", 1 ],
			"obj-4::obj-19::obj-65" : [ "Transp", "Transp", 0 ],
			"obj-4::obj-19::obj-82" : [ "Gain", "Gain", 0 ],
			"obj-4::obj-28" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-8::obj-15" : [ "live.gain~", "live.gain~", 0 ],
			"obj-9::obj-15" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-9::obj-4" : [ "live.gain~[1]", "live.gain~[1]", 0 ],
			"parameterbanks" : 			{

			}
,
			"parameter_overrides" : 			{
				"obj-1::obj-15" : 				{
					"parameter_longname" : "live.gain~[3]"
				}
,
				"obj-4::obj-28" : 				{
					"parameter_longname" : "live.gain~[4]"
				}
,
				"obj-9::obj-15" : 				{
					"parameter_longname" : "live.gain~[2]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "20170816 - Bloc-marque Culture couleur.tiff",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "IRCAM.CP.tiff",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO CNRS 2019_CMJN.tiff",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO_SU_HORIZ_SEUL_CMJN color.tiff",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "bonus recorder.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "draw-trap-grain.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "grain-hann.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "grain-visualization.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "iMubu-large.png",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "logo-ircamforum.png",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "mubu logo.tiff",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/media",
				"patcherrelativepath" : "../media",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-concat.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-granular.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-live-granular.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-play-credits.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Lesson3 no grains/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu.concat~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.granular~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "pipo.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "IRCAM",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 14.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "monacy",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 9.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
