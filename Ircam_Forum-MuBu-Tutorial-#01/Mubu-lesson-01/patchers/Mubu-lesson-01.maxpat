{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 2,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"openrect" : [ 34.0, 87.0, 1724.0, 999.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 9.0,
		"default_fontface" : 0,
		"default_fontname" : "Monaco",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 1724.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "monacy",
		"subpatcher_template" : "monaco_template",
		"showrootpatcherontab" : 0,
		"showontab" : 0,
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 469.0, 47.0, 100.0, 20.0 ],
					"text" : "\"mubu 01-02\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 363.0, 47.0, 100.0, 20.0 ],
					"text" : "\"mubu 01-01\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 261.0, 47.0, 100.0, 20.0 ],
					"text" : "\"mubu 01-00a\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 22.0, 47.0, 118.0, 20.0 ],
					"text" : "mubu-credits.maxpat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 150.0, 47.0, 100.0, 20.0 ],
					"text" : "\"mubu 01-00\""
				}

			}
 ],
		"lines" : [  ],
		"parameters" : 		{
			"obj-4::obj-22" : [ "live.gain~", "live.gain~", 0 ],
			"parameterbanks" : 			{

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "20170816 - Bloc-marque Culture couleur.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "IRCAM.CP.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO CNRS 2019_CMJN.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "LOGO_SU_HORIZ_SEUL_CMJN color.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "iMubu-large.png",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/z-logos",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/z-logos",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "logo-ircamforum.png",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/z-logos",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/z-logos",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "mubu 01-00.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson 1",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/lesson 1",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu 01-00a.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson 1",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/lesson 1",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu 01-01.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson 1",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/lesson 1",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu 01-02.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson 1",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/lesson 1",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu logo.tiff",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/credits patch",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/credits patch",
				"type" : "TIFF",
				"implicit" : 1
			}
, 			{
				"name" : "mubu-credits.maxpat",
				"bootpath" : "~/Documents/Max 8/clients/MuBu_lesson_patches/lesson 1",
				"patcherrelativepath" : "../../../clients/MuBu_lesson_patches/lesson 1",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu.granular~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "monacy",
				"default" : 				{
					"fontname" : [ "Monaco" ],
					"fontsize" : [ 9.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
